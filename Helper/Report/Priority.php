<?php
/**
 * Created by PhpStorm.
 * User: Yusuf
 * Date: 05/06/2018
 * Time: 10:41
 */

namespace Penta\ResearchDevelopment\Helper\Report;

interface Priority
{
	const PRIORITY_LOW    = 1;

	const PRIORITY_MEDIUM = 2;

	const PRIORITY_HIGH   = 3;

	const PRIORITY_URGENT = 4;
}
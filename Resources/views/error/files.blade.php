<div class="ticket-header">
    <h4>Dosyalar</h4>
</div>

<div class="ticket-info-body">
    <table class="table table-hover">
        <tbody>
        @forelse($report->files as $file)
            <tr>
                <th>
                    {!! $file->getFileForDetail($width ?? '120px')  !!}
                    {!! $file->original_name  !!}
                </th>
                <td class="float-xs-right" style="border:none;">
                    <a href="{{ route('researchdevelopment.errors.file.destroy',$file->id) }}"
                       onclick="return confirm('Emin misiniz ?')">
                        <i class="fa fa-trash danger"></i>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td class="text-muted">Dosya mevcut değil !</td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>
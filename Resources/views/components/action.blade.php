<div class="btn-group">
    <button type="button"
            class="btn btn-sm btn-outline-secondary no-action cursor-pointer"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false">
        <i class="fa fa-ellipsis-h"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-left">
        {{ $show or '' }}

        {{ $edit or '' }}

        {{ $delete or '' }}

        {{ $slot or '' }}
    </div>
</div>


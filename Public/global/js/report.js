$(function () {

    let token = $('meta[name="csrf-token"]').attr('content');

    $('button.update_status_trigger').on('click', function () {

        $('.update-priority').removeClass('drop-active');
        $('.update-status').toggleClass('drop-active');
    });

    $('button.update_priority_trigger').on('click', function () {
        $('.update-status').removeClass('drop-active');
        $('.update-priority').toggleClass('drop-active');
    });

    $('.up-box-cancel').on('click', function () {
        var $this = $(this);
        $this.closest('.up-info-box').removeClass('drop-active');
    });

    $('input[name="status"]').on('change', function (e) {
        e.preventDefault();

        var $form = $('.ticket_status_form');

        var $data = $form.serialize();
        var $url = $form.attr('action');

        $.post($url, $data, function (resp) {
            $('td.status').html(resp.status);
        }).fail(function (resp) {
            toastr.error(resp.fail);
        });

    });

    $('input[name="priority"]').on('change', function (e) {
        e.preventDefault();

        var $form = $('.ticket_priority_form');

        var $data = $form.serialize();
        var $url = $form.attr('action');

        $.post($url, $data, function (resp) {
            $('td.priority').html(resp.priority);
        }).fail(function (resp) {
            toastr.error(resp.fail);
        });

    });


    $('form.reply_form').on('submit', function (e) {

        var $reply = $('textarea', this);

        var $form = $(this);

        var $data = new FormData($form[0]);

        if ($reply.val().trim() == '') {
            var $emptyMessage = 'Lütfen <strong>yanıt</strong> alanını boş bırakmayınız';

            addSubmitStatus('alert-danger', $emptyMessage);
        } else {

            resetSubmitStatus('alert-danger');

            var $sendMessage = '<i class="fa fa-spinner fa-pulse fa-fw"></i> Gönderiliyor';

            addSubmitStatus('alert-warning', $sendMessage);

            var $url = $form.attr('action');

            $.ajax({
                url: $url,
                type: 'POST',
                data: $data,
                async: false,
                success: function (resp) {
                    resetSubmitStatus('alert-warning');

                    addSubmitStatus('alert-success', 'Gönderildi.');

                    setTimeout(function () {

                        resetSubmitStatus('alert-success');
                        $('html,body').animate({
                            scrollTop: ($('.all-replies .single-reply:first').offset().top) - 100
                        }, 1000);

                    }, 2000);

                    $('.all-replies').prepend(resp.reply_body);
                    $('.all-replies .single-reply:first').hide().fadeIn('slow');

                    $form[0].reset();
                },
                error: function () {
                    toastr.error("Bir hata oluştu");
                    resetSubmitStatus('alert-warning');
                },
                processData: false,
                contentType: false,
            });

        }

        e.preventDefault();


    });

    $('body').on('click', '.remove-reply', function () {

        var $element = $(this).closest('.single-reply');

        var $url = $(this).data('url');

        var $data = {
            _token: token,
            _method: 'DELETE'
        };

        $.post($url, $data, function (resp) {
            $element.fadeOut(850);
            setTimeout(function () {
                $element.remove();
            }, 851);
        }).fail(function (resp) {
            toastr.error("Bir hata oluştu");
        });

    });

    $('body').on('click', '.edit-reply', function () {
        var $url = $(this).data('url');

        var $data = {
            _token: token
        };

        $.get($url, $data, function (resp) {

            $('#editReply form').attr('action', resp.action);
            $('#editReply form textarea').val(resp.reply_body);

            $('#editReply').modal('show');

        }).fail(function (resp) {
            toastr.error('Bir hata oluştu');
        });

    });


    function resetSubmitStatus(removeClass) {
        $('.submit_status').fadeOut('slow').html('').addClass('hidden').removeClass(removeClass);
    }

    function addSubmitStatus(addClass, addMessage) {
        $('.submit_status').removeClass('hidden').hide().fadeIn('slow').addClass(addClass).html(addMessage);
    }
});

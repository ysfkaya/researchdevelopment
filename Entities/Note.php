<?php

namespace Penta\ResearchDevelopment\Entities;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
	protected $fillable = [
		'body',
	];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function bugs()
	{
		return $this->morphedByMany(ErrorReport::class, 'noteable');
	}
}

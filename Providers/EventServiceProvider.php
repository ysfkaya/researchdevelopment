<?php

namespace Penta\ResearchDevelopment\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Penta\ResearchDevelopment\Events\ReportWasCreated;
use Penta\ResearchDevelopment\Listeners\NotifyAdminsOfANewReport;

class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		ReportWasCreated::class => [
			NotifyAdminsOfANewReport::class,
		],
	];
}

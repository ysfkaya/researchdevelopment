<div class="modal fade" id="show_note_content" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header  mt-2">
                <h5 class="modal-title text-center mt-0 mb-0 m-auto">
                    İçerik
                </h5>
                <button type="button" class="close ml-0 font-bold font-size-large position-relative cursor-pointer"
                        style="top: -25px;"
                        data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color: #E6F3F8"></div>
        </div>
    </div>
</div>



<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorReportsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('error_reports', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger(config('researchdevelopment.user.reference_id'));
			$table->string('subject');
			$table->smallInteger('priority');
			$table->text('body');
			$table->smallInteger('status')->default(3);
			$table->string('ip_address');
			$table->timestamps();

			$table->foreign(config('researchdevelopment.user.reference_id'))
				->references('id')
				->on(config('researchdevelopment.user.table'))
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('error_reports');
	}
}

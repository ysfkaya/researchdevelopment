<?php

namespace Penta\ResearchDevelopment\Helper\Report;

interface Status
{
	const NOT_STARTED = 1;

	const IN_PROGRESS = 2;

	const ON_HOLD     = 3;

	const CANCELED    = 4;

	const FINISHED    = 5;

	const OPEN        = 6;

	const ANSWER      = 7;

	const IN_ACTIVE   = 0;
}
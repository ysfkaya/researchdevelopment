<?php

namespace Penta\ResearchDevelopment\Entities;

use App\Admin;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Penta\ResearchDevelopment\Helper\StatusInterface;
use Penta\ResearchDevelopment\Helper\StatusTrait;

class Development extends Model implements StatusInterface
{
	use StatusTrait;

	const STATUS_PENDING_PRICING = 'pending_pricing';

	const STATUS_PRICING         = 'pricing';

	protected $fillable = [
		'description',
		'row',
		'title',
		'section',
		'price',
		'price_desc',
		'status',
		'user_id',
	];

	public function user()
	{
		if (Schema::hasTable('admins') && class_exists(Admin::class)) {
			return $this->hasOne(Admin::class, 'id', 'user_id');
		} else {
			return $this->hasOne(User::class, 'id', 'user_id');
		}
	}

	public function getStatusWithHtml()
	{
		switch ($this->attributes['status']) {
			case self::STATUS_PENDING_APPROVED:
				return $this->htmlButton('warning', 'Onay Bekliyor');
			case self::STATUS_APPROVED:
				return $this->htmlButton('primary', 'Onaylandı');
			case self::STATUS_UNAPPROVED:
				return $this->htmlButton('danger', 'Onaylanmadı');
			case self::STATUS_IN_PROCESS:
				return $this->htmlButton('blue-grey', 'İşlemde');
			case self::STATUS_SOLVED:
				return $this->htmlButton('success', 'Tamamlandı');
			case self::STATUS_UNSOLVED:
				return $this->htmlButton('pink', 'Yapılmadı');
			case self::STATUS_PENDING_PRICING:
				return $this->htmlButton('facebook', 'Fiyatlandırma Bekleniyor');
			case self::STATUS_PRICING:
				return $this->htmlButton('google', 'Fiyatlandırıldı').
					" <button data-toggle='tooltip' title='Fiyatı Gör' class='btn btn-sm no-action cursor-pointer btn-primary show_price' href='".
					route('arge.development.showPricing', $this->id)."'><i class='fa fa-eye'></i></button>";
			default:
				return $this->htmlButton('blue', 'Tanımsız');
		}
	}
}

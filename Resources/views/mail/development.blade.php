@extends('researchdevelopment::layouts.mail')

@section('title',$title = config('researchdevelopment.name').' - Geliştirme Talebi')

@section('content')
    <div class="movableContent"
         style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td height="35"></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0"
                           cellpadding="0">
                        <tbody>
                        <tr>
                            <td valign="top" class="specbundle">
                                <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable">
                                        <p style='text-align:center;margin:0;font-family:Georgia,Time,sans-serif;font-size:26px;color:#DC2828;'>
                                        <span class="font">
                                        {{ $title }}
                                        </span>
                                        </p>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="movableContent"
         style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0"
               align="center">
            <tr>
                <td valign='top' align='center'>
                    <div class="contentEditableContainer contentImageEditable">
                        <div class="contentEditable">
                            <img src="{{ asset('vendor/researchdevelopment/mail/line.png') }}" width='251'
                                 height='43' alt=''
                                 data-default="placeholder"
                                 data-max-width="560">
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="movableContent"
         style="border: 0px; padding-top: 0px; position: relative;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0"
               align="center">
            <tr>
                <td height='55'></td>
            </tr>
            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>
                            <h2>{{ config('researchdevelopment.href').' yeni bir geliştirme talebi gönderildi' }}</h2>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td height='15'></td>
            </tr>

            <tr>
                <td align='left'>
                    <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable" align='center'>
                            <p>
                                {!! $content !!}
                            </p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td height='20'></td>
            </tr>
        </table>
    </div>
@endsection

<div class="form-group">
    <label for="title" class="control-label control-label-required">Bölüm</label>
    <div class="clearfix"></div>
    <select name="section" class="form-control select2-placeholder" data-width="100%">
        <option value=""></option>
        @foreach(config('researchdevelopment.map') as $key => $value)
            <optgroup label="{{ $key }}">
                @foreach($value as $k => $v)
                    @include('researchdevelopment::map.recursive',['map' => $v,'key' => $k,'count' => 0])
                @endforeach
            </optgroup>
        @endforeach
    </select>
    @if($errors->has('section'))
        <p class="help-block danger">
            {{ $errors->first('section') }}
        </p>
    @elseif($errors->edit->all())
        <p class="help-block danger">
            {{ $errors->edit->first('section') }}
        </p>
    @endif
</div>
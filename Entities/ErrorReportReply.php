<?php

namespace Penta\ResearchDevelopment\Entities;

use Illuminate\Database\Eloquent\Model;

class ErrorReportReply extends Model
{
	protected $guarded = [];

	public function user()
	{
		return $this->belongsTo(config('researchdevelopment.user.class'), config('researchdevelopment.user.reference_id'));
	}

	public function admin()
	{
		return $this->belongsTo(config('researchdevelopment.admin.class'), config('researchdevelopment.admin.reference_id'));
	}

	public function getRepliedDate()
	{
		return $this->created_at->toDateTimeString() === $this->updated_at->toDateTimeString() ? $this->created_at->diffForHumans()
			: $this->updated_at->diffForHumans().' güncellendi';
	}

	public function getCustomerOrUserAttribute()
	{
		return $this->attributes['user_id'] ? $this->user->full_name : $this->admin->full_name;
	}

	public function body(bool $customer = false)
	{
		$reply = $customer === false ? $this->admin : $this->user;

		$role = $customer === false ? optional($reply->roles()->first())->display_name : 'Müşteri';

		$removeRoute = $customer === false ? route('researchdevelopment.errors.replyDestroy', $this->id) : null;
		$editRoute = $customer === false ? route('researchdevelopment.errors.replyEdit', $this->id) : null;

		return '<div class="single-reply">
            <div class="media">
                <div class="media-left reply-user-img">
                </div>
                <div class="media-body reply-user-details">
                    <h4 class="media-heading">
                        '.$reply->full_name.'
                        <button class="edit-reply pull-right" data-url="'.$editRoute.'"><i class="fa fa-edit"></i></button>
                        <button class="remove-reply pull-right" data-url="'.$removeRoute.'"><i class="fa fa-times"></i></button>
                    </h4>
                    <div class="row">
                        <p class="pull-left user-role">'.$role.'</p>
                        <p class="pull-right reply-date">
                            '.$this->created_at->diffForHumans().'
                        </p>
                    </div>
                    <div class="reply-message">
                        <p>
                            '.$this->reply_body.'
						</p>
                    </div>
                </div>
            </div>
        </div>';
	}
}

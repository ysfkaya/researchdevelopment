@extends(config('researchdevelopment.view.extends'))

@section(config('researchdevelopment.view.body'))

    @component('researchdevelopment::components.content')

        @slot('head')
            Hata Raporu Oluştur
        @endslot


        @slot('body')
            {!! BootForm::open()->action(route('researchdevelopment.errors.store'))->class('form')->enctype('multipart/form-data') !!}
            <input type="hidden" name="user_id" value="{{ auth()->id() }}">
            <div class="card-block">
                <div class="form-body">
                    {!! BootForm::text('Konu', 'subject')->required() !!}
                    {!! BootForm::select('Öncelik', 'priority')->required()->options($priorities) !!}
                    {!! BootForm::textarea('Açıklama', 'body')->required() !!}
                    {!! BootForm::file('Dosya', 'files[]')->multiple()->class('form-control') !!}
                </div>
                <div class="form-actions center">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Kaydet
                    </button>
                </div>
            </div>
            {!! BootForm::close() !!}
        @endslot
    @endcomponent

@endsection

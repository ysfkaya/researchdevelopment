<?php

namespace Penta\ResearchDevelopment\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Penta\ResearchDevelopment\Entities\Development;

class DevelopmentMail extends Mailable implements ShouldQueue
{
	use Queueable, SerializesModels;

	public $development;

	/**
	 * Create a new message instance.
	 *
	 * @param \Penta\ResearchDevelopment\Entities\Development $development
	 */
	public function __construct(Development $development)
	{
		$this->development = $development;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->subject('Geliştirme Talebi')->view('researchdevelopment::mail.development', ['content' => $this->development->description]);
	}
}

<div class="ticket-header">
    <h4>Destek Bilgisi</h4>
</div>

<div class="ticket-info-body">
    <table class="table table-hover">
        <tbody>
        <tr>
            <th> ID:</th>
            <td>#{{ $report->id }}</td>
        </tr>
        <tr>
            <th>Oluşturan :</th>
            <td>{{ optional($report->user)->full_name }}</td>
        </tr>
        <tr>
            <th>Durum :</th>
            <td class="status">
                {!! $report->getStatus()  !!}
            </td>
        </tr>
        <tr>
            <th>Öncelik :</th>
            <td class="priority">
                {!! $report->getPriority() !!}
            </td>
        </tr>
        </tbody>
    </table>
</div>

@role(config('researchdevelopment.roles.developer'))
<div class="ticket-info-footer">
    <div class="ticket-info-buttons">
        <div class="as-n-s">
            <button class="btn btn-blue-grey p-1 update_priority_trigger">Öncelik Güncelle</button>
            <div class="up-info-box update-priority">
                <h3>Talep Önceliğini Güncelle</h3>
                <span class="up-box-cancel"><i class="fa fa-times"></i></span>
                <span class="up-box-status statusUpBx"><i
                            class="fa fa-circle-o-notch fa-spin status-success"></i></span>

                <!-- UPDATE TICKET STATUS POPUP -->
                <div class="box-form-outer">
                {!! BootForm::open()->action(route('researchdevelopment.errors.priority.update',$report->id))->class('form ticket_priority_form')->put() !!}
                <!-- UPDATE STATUS LIST -->
                    <div class="media">
                        <label for="ticket_priority_low">
                            <div class="media-body">
                                <h4 class="media-heading">Düşük</h4>
                                <div>
                                    <input type="radio" name="priority"
                                           class="ticket_priority {{ ($hasLow = $report->pripority === $report::PRIORITY_LOW) ? 'checked' : null }}"
                                           value="{{ $report::PRIORITY_LOW }}"
                                           {{ $hasLow ? 'checked' : null }}
                                           id="ticket_priority_low">
                                    <span class="input_radio ticket_low"></span>
                                </div>
                            </div>
                        </label>
                    </div>

                    <!-- UPDATE STATUS LIST -->
                    <div class="media">
                        <label for="ticket_priority_medium">

                            <div class="media-body">
                                <h4 class="media-heading">Normal</h4>
                                <div>
                                    <input type="radio" name="priority"
                                           class="ticket_priority {{ ($hasMedium = $report->priority === $report::PRIORITY_MEDIUM) ? 'checked' : null }}"
                                           {{ $hasMedium ? 'checked' : null }}
                                           value="{{ $report::PRIORITY_MEDIUM }}"
                                           id="ticket_priority_medium"
                                    >
                                    <span class="input_radio ticket_medium"></span>
                                </div>
                            </div>
                        </label>
                    </div>

                    <!-- UPDATE STATUS LIST -->
                    <div class="media">
                        <label for="ticket_priority_high">
                            <div class="media-body">
                                <h4 class="media-heading">Yüksek</h4>
                                <div>
                                    <input type="radio" name="priority"
                                           class="ticket_priority {{ ($hasHigh = $report->priority === $report::PRIORITY_HIGH) ? 'checked' : null }}"
                                           {{ $hasHigh ? 'checked' : null }}
                                           value="{{ $report::PRIORITY_HIGH }}"
                                           id="ticket_priority_high">
                                    <span class="input_radio ticket_high"></span>
                                </div>
                            </div>
                        </label>
                    </div>

                    <div class="media">
                        <label for="ticket_priority_urgent">
                            <div class="media-body">
                                <h4 class="media-heading">Acil</h4>
                                <div>
                                    <input type="radio" name="priority"
                                           class="ticket_priority {{ ($hasUrgent = $report->priority === $report::PRIORITY_URGENT) ? 'checked' : null }}"
                                           {{ $hasUrgent ? 'checked' : null }}
                                           value="{{ $report::PRIORITY_URGENT }}"
                                           id="ticket_priority_urgent">
                                    <span class="input_radio ticket_urgent"></span>
                                </div>
                            </div>
                        </label>
                    </div>

                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>

        <div class="as-n-u float-xs-right">
            <button class="btn btn-success p-1 update_status_trigger">Durum Güncelle</button>

            <div class="up-info-box update-status">
                <h3>Talep Durumu Güncelle</h3>
                <span class="up-box-cancel"><i class="fa fa-times"></i></span>
                <span class="up-box-status statusUpBx"><i
                            class="fa fa-circle-o-notch fa-spin status-success"></i></span>

                <!-- UPDATE TICKET STATUS POPUP -->
                <div class="box-form-outer">
                {!! BootForm::open()->action(route('researchdevelopment.errors.status.update',$report->id))->class('form ticket_status_form')->put() !!}
                <!-- UPDATE STATUS LIST -->
                    <div class="media">
                        <label for="ticket_status_solved">
                            <div class="media-body">
                                <h4 class="media-heading">Çözüldü</h4>
                                <div>
                                    <input type="radio" name="status"
                                           class="ticket_status {{ $report->status === $report::FINISHED ? 'checked' : null }}"
                                           value="{{ $report::FINISHED }}"
                                           {{ $report->status === $report::FINISHED ? 'checked' : null }}
                                           id="ticket_status_solved">
                                    <span class="input_radio"></span>
                                </div>
                            </div>
                        </label>
                    </div>

                    <!-- UPDATE STATUS LIST -->
                    <div class="media">
                        <label for="ticket_status_pending">

                            <div class="media-body">
                                <h4 class="media-heading">Beklemede</h4>
                                <div>
                                    <input type="radio" name="status"
                                           class="ticket_status {{ $report->status === $report::ON_HOLD ? 'checked' : null }}"
                                           {{ $report->status === $report::ON_HOLD ? 'checked' : null }}
                                           value="{{ $report::ON_HOLD }}" id="ticket_status_pending"
                                    >
                                    <span class="input_radio ticket_pending"></span>
                                </div>
                            </div>
                        </label>
                    </div>

                    <!-- UPDATE STATUS LIST -->
                    <div class="media">
                        <label for="ticket_status_reply">

                            <div class="media-body">
                                <h4 class="media-heading">Yanıtlandı</h4>
                                <div>
                                    <input type="radio" name="status"
                                           class="ticket_status {{ $report->status === $report::ANSWER ? 'checked' : null }}"
                                           {{ $report->status === $report::ANSWER ? 'checked' : null }}
                                           value="{{ $report::ANSWER }}" id="ticket_status_reply">
                                    <span class="input_radio ticket_reply"></span>
                                </div>
                            </div>
                        </label>
                    </div>

                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endrole

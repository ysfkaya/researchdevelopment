<?php

return [
	// Site Name
	'name'       => '',
	// Site Address
	'href'       => '',

	// Middleware
	'middleware' => '',

	// Route prefix
	'prefix'     => '',

	// View Layouts
	'view'       => [
		'extends' => '',
		'body'    => '',
		'js'      => '',
		'css'     => '',
	],
	// When created Development or Error Report send mail
	'mails'      => [],

	'roles' => [
		'developer' => '',
		'owner'     => '',
		'couple'    => [],
	],

	'storage_disk' => 'public',

	'user' => [
		'class'        => \App\User::class,
		'table'        => 'users',
		'reference_id' => 'user_id',
	],

	'admin' => [
		'class'        => \App\User::class,
		'table'        => 'users',
		'reference_id' => 'admin_id',
	],
];

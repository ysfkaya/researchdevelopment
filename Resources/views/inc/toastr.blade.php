<script>
    $(function () {
        @if(session()->has('toastr_success'))
        toastr.success('{{ session()->get('toastr_success') }}', '{{ session()->get('toastr_title') }}', {
            positionClass: 'toast-bottom-right',
            containerId: 'toast-bottom-right'
        });
        @endif
        @if(session()->has('toastr_error'))
        toastr.error('{{ session()->get('toastr_error') }}', '{{ session()->get('toastr_title') }}', {
            positionClass: 'toast-bottom-right',
            containerId: 'toast-bottom-right'
        });
        @endif
        @if(session()->has('toastr_warning'))
        toastr.warning('{{ session()->get('toastr_warning') }}', '{{ session()->get('toastr_title') }}', {
            positionClass: 'toast-bottom-right',
            containerId: 'toast-bottom-right'
        });
        @endif
        @if(session()->has('toastr_info'))
        toastr.info('{{ session()->get('toastr_info') }}', '{{ session()->get('toastr_title') }}', {
            positionClass: 'toast-bottom-right',
            containerId: 'toast-bottom-right'
        });
        @endif
    });
</script>
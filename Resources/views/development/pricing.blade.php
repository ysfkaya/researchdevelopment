<div class="modal fade text-left" id="pricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Fiyatlandırma</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! BootForm::open()->action('')->class('form')->put() !!}
            {!! BootForm::hidden('pricing_action')->class('pricing_action_class')  !!}
            <div class="modal-body">
                {!! BootForm::text('Fiyat', 'price')->required()->type('number') !!}
                @if($errors->price->has('price'))
                    <p class="help-block danger">
                        {{ $errors->price->first('price') }}
                    </p>
                @endif
                {!! BootForm::textarea('Açıklama', 'price_desc') !!}
                @if($errors->has('price_desc'))
                    <p class="help-block danger">
                        {{ $errors->first('price_desc') }}
                    </p>
                @endif
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-outline-primary" value="Kaydet">
            </div>
            {!! BootForm::close() !!}
        </div>
    </div>
</div>


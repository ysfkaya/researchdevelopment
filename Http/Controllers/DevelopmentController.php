<?php

namespace Penta\ResearchDevelopment\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Penta\ResearchDevelopment\Entities\Development;
use Penta\ResearchDevelopment\Mail\DevelopmentMail;
use Yajra\DataTables\DataTables;

class DevelopmentController extends Controller
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('researchdevelopment::development.index');
	}

	/**
	 * @return mixed
	 * @throws \Exception
	 */
	public function data()
	{

		$developmentNotes = Development::query()->orderByRaw('case when status = \'pending_approved\' then 1
	              when status = \'approved\' then 2
	              when status = \'in_process\' then 3
	              when status = \'unsolved\' then 4
	              when status = \'unapproved\' then 5
	              else 6
	            end asc')->with('user');

		return DataTables::of($developmentNotes)->addColumn('action', function ($development) {
			return view('researchdevelopment::development.action', compact('development'));
		})->editColumn('status', function ($development) {
			return $development->getStatusWithHtml();
		})->filterColumn('status', function ($query, $keyword) {
			$sql = "IF(status = ?,'Onay Bekliyor',";
			$sql .= "IF(status = ?,'Onaylandı',";
			$sql .= "IF(status = ?,'Onaylanmadı',";
			$sql .= "IF(status = ?,'İşlemde',";
			$sql .= "IF(status = ?,'Tamamlandı',";
			$sql .= "IF(status = ?,'Fiyatlandırma Bekleniyor',";
			$sql .= "IF(status = ?,'Fiyatlandırıldı',";
			$sql .= "IF(status = ?,'Yapılmadı','')))))))) like ?";

			$query->whereRaw($sql, [
				Development::STATUS_PENDING_APPROVED,
				Development::STATUS_APPROVED,
				Development::STATUS_UNAPPROVED,
				Development::STATUS_IN_PROCESS,
				Development::STATUS_SOLVED,
				Development::STATUS_PENDING_PRICING,
				Development::STATUS_PRICING,
				Development::STATUS_UNSOLVED,
				'%'.$keyword.'%',
			]);
		})->editColumn('created_at', function ($development) {
			return $development->created_at->diffForHumans();
		})->rawColumns(['status', 'action'])->make(true);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'title'       => 'required|max:191',
			'description' => 'required',
		], [
			'title.required'       => 'Lütfen bir başlık giriniz.',
			'title.max'            => 'Başlık alanı maksimum 191 karakter olabilir. Karakter sayısı '.strlen($request->get('title')),
			'description.required' => 'Lütfen bir açıklama belirtiniz.',
		]);

		$attributes = $request->all();
		$attributes['user_id'] = auth()->id();

		$create = Development::create($attributes);

		Mail::to(config('researchdevelopment.mails', []))->send(new DevelopmentMail($create));

		return back()->with('toastr_success', 'Geliştirme Talebi Oluşturuldu');
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function show(Request $request)
	{
		if ($request->ajax()) {
			$id = (int)$request->get('id');

			$development = Development::findOrFail($id);

			return response()->json(['success' => true, 'description' => $development->description, 'title' => $development->title]);
		}

		abort(404);
	}

	/**
	 * @param \Penta\ResearchDevelopment\Entities\Development $development
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function edit(Development $development)
	{
		if ( ! \request()->ajax()) {
			abort(404);
		}

		$attributes = $development->toArray();
		$attributes['action'] = route('researchdevelopment.developments.updateEdit', $development);

		return response()->json($attributes);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param \Penta\ResearchDevelopment\Entities\Development $development
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function updateEdit(Request $request, Development $development)
	{
		$this->validateWithBag('edit', $request, [
			'title'            => 'required|max:191',
			'edit_description' => 'required',
			'row'              => 'nullable|integer',
			'section'          => 'required',
		], [
			'title.required'            => 'Lütfen bir başlık giriniz.',
			'title.max'                 => 'Başlık alanı maksimum 191 karakter olabilir. Karakter sayısı '.strlen($request->get('title')),
			'edit_description.required' => 'Lütfen bir açıklama belirtiniz.',
			'section.required'          => 'Lütfen bir bölüm belirtiniz.',
			'row.integer'               => 'Sıra numarası sadece sayısal değer olabilir.',
		]);

		$attributes = $request->except('action');
		$attributes['row'] = empty($request->get('row')) ? '999' : $request->get('row');
		$attributes['description'] = $request->get('edit_description');

		$update = $development->update($attributes);
		if ($update) {

			return back()->with('toastr_success', 'Geliştirme notu güncellendi.');
		}

		return back()->with('toastr_error', 'Geliştirme notu güncellenemedi');
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function destroy($id)
	{
		Development::destroy($id);

		return response()->json(['messsage' => 'Deleted']);
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function showPricing($id)
	{
		$development = Development::findOrFail($id);

		return response()->json([
			'success'     => true,
			'development' => $development,
		]);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function updatePricing(Request $request, $id)
	{
		$this->validateWithBag('price', $request, ['price' => 'required']);

		Development::findOrFail($id)->update([
			'price'      => $request->get('price'),
			'status'     => Development::STATUS_PRICING,
			'price_desc' => $request->get('price_desc'),
		]);

		return back()->with('toastr_success', 'Fiyatlandırma Uygulandı');
	}

	/**
	 * @param $id
	 * @param $status
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updateStatus($id, $status)
	{
		$development = Development::findOrFail($id);

		$attributes = ['status' => $status];

		$development->update($attributes);

		if ($status === Development::STATUS_APPROVED) {
			Mail::to(config('oguslarkia.mails'))->send(new ApprovedMail($development));
		}

		return response()->json(['message' => 'Updated']);
	}
}

<?php

namespace Penta\ResearchDevelopment\Helper;

use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

trait FileCreatorTrait
{
	/**
	 * @var array|\Illuminate\Support\Collection
	 */
	public $fileAttributes;

	/**
	 * @var string
	 */
	public $savePath;

	/**
	 * @var UploadedFile
	 */
	public $file;

	/**
	 * @param array|\Illuminate\Http\UploadedFile $file
	 *
	 * @param array $extraAttributes
	 *
	 * @return $this
	 */
	public function file(UploadedFile $file, array $extraAttributes = [])
	{
		$this->file = $file;
		$this->savePath = public_path('uploads');

		$this->fileAttributes = collect([
			'original_name' => $this->file->getClientOriginalName(),
			'hash_name'     => str_random(40).'.'.$this->file->getClientOriginalExtension(),
			'type'          => $this->file->getMimeType(),
			'extension'     => $this->file->getClientOriginalExtension(),
			'size'          => $this->file->getSize(),
			'tmp'           => $this->file->getRealPath(),
			'file'          => $this->file,
		]);

		foreach ($extraAttributes as $key => $value) {
			$this->fileAttributes->put($key, $value);
		}

		return $this;
	}

	/**
	 * Upload and insert into the database
	 *
	 * @param array $config
	 *
	 * @return mixed
	 */
	public function upload(array $config = [])
	{
		// save path with file name
		$filePath = $this->sparePath().$this->fileAttributes->get('hash_name');

		$imgType = $this->fileAttributes->get('type');

		if ( ! starts_with($imgType, 'image')) {
			return null;
		}
		// create
		$image = Image::make($this->fileAttributes->get('tmp'));
		// resize
		if (isset($config['width']) || isset($config['height'])) {
			$image->resize($config['width'], $config['height']);
		}
		// save
		$image->save($filePath, $config['quality'] ?? null);
		// put for database
		$this->put($filePath);

		return $this->fileAttributes->toArray();
	}

	/**
	 * Set full path
	 *
	 * @param $path
	 *
	 * @return $this
	 */
	public function setSaveFile(string $path)
	{
		$this->savePath = $path;

		return $this;
	}

	/**
	 * Url parser for db
	 *
	 * @return null|string
	 */
	private function parseUrl()
	{
		$filePath = $this->sparePath().$this->fileAttributes->get('hash_name');

		if (app()->environment() !== 'production') {
			$search = 'public';
		} else {
			$search = 'public_html/main/public';
		}

		if (str_contains($filePath, $search)) {
			return str_replace('\\', '/', str_after($filePath, $search));
		}

		return null;
	}

	public function fileDestroy(string $path, callable $callback)
	{
		if (\File::isFile($path)) {
			\File::delete($path);
		}

		$callback();
	}

	/**
	 * @param string $filePath
	 */
	private function put(string $filePath)
	{
		$this->fileAttributes->put('real_path', $filePath);
		$this->fileAttributes->put('url', $this->parseUrl());
	}

	private function getDirectory()
	{
		$now = Carbon::now();

		$dir = [
			$now->year,
			title_case($now->format('F')),
		];

		$directory = implode('-', $dir).DIRECTORY_SEPARATOR;

		$path = $this->savePath.DIRECTORY_SEPARATOR.$directory;

		if ( ! \File::isDirectory($path)) {
			\File::makeDirectory($path);
		}

		return $directory;
	}

	/**
	 * @return string
	 */
	private function sparePath(): string
	{
		return $this->savePath.DIRECTORY_SEPARATOR.$this->getDirectory();
	}
}
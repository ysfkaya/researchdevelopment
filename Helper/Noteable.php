<?php

namespace Penta\ResearchDevelopment\Helper;

use Penta\ResearchDevelopment\Entities\Note;

trait Noteable
{
	/**
	 * @return mixed
	 */
	public function notes()
	{
		return $this->morphToMany(Note::class, 'noteable');
	}

	public static function boot()
	{
		parent::boot();

		static::deleted(function ($affected) {
			$affected->notes()->delete();
		});
	}
}
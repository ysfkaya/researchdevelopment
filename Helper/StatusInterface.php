<?php

namespace Penta\ResearchDevelopment\Helper;

interface StatusInterface
{
	const STATUS_PENDING_APPROVED = 'pending_approved';

	const STATUS_APPROVED         = 'approved';

	const STATUS_UNAPPROVED       = 'unapproved';

	const STATUS_IN_PROCESS       = 'in_process';

	const STATUS_SOLVED           = 'solved';

	const STATUS_UNSOLVED         = 'unsolved';
}
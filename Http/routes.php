<?php
/*
 |--------------------------------------------------------------------------
 |  R & D Group
 |--------------------------------------------------------------------------
*/
Route::group([
	'middleware' => config('researchdevelopment.middleware'),
	'prefix'     => config('researchdevelopment.prefix'),
	'namespace'  => 'Penta\ResearchDevelopment\Http\Controllers',
	'as'         => 'researchdevelopment.',
], function () {

	/*
	 |--------------------------------------------------------------------------
	 |  Development Group
	 |--------------------------------------------------------------------------
	*/
	Route::group(['prefix' => 'developments', 'as' => 'developments.'], function () {

		// Index
		Route::get('/', 'DevelopmentController@index')->name('index');
		// Data Table
		Route::get('/data', 'DevelopmentController@data')->name('data');
		// Store
		Route::post('/yeni-not', 'DevelopmentController@store')->name('store');
		// Show
		Route::post('/goster', 'DevelopmentController@show')->name('show');
		// Edit
		Route::get('/duzenle/{development}', 'DevelopmentController@edit')->name('edit');
		// Update Edit
		Route::put('/duzenle/{development}', 'DevelopmentController@updateEdit')->name('updateEdit');
		// Destroy
		Route::post('/sil/{id}', 'DevelopmentController@destroy')->name('destroy');

		// Show Pricing
		Route::get('/fiyat/{id}', 'DevelopmentController@showPricing')->name('showPricing');
		// Update Pricing
		Route::put('/fiyatlandir/{id}', 'DevelopmentController@updatePricing')->name('updatePricing');

		// Update Status
		Route::post('/durum-guncelle/{id}/{status}', 'DevelopmentController@updateStatus')->name('update.status');
	});

	/*
	 |--------------------------------------------------------------------------
	 |  Research | Error(Bug) Reports Group
	 |--------------------------------------------------------------------------
	*/
	Route::any('errors/data', 'ErrorController@data')->name('errors.data');
	Route::any('errors/file/destroy/{file}', 'ErrorController@fileDestroy')->name('errors.file.destroy');
	Route::put('errors/priority/update/{id}', 'ErrorController@updatePriority')->name('errors.priority.update');
	Route::put('errors/status/update/{id}', 'ErrorController@updateStatus')->name('errors.status.update');
	Route::post('errors/reply/{id}', 'ErrorController@reply')->name('errors.reply');
	Route::get('errors/edit/reply/{id}', 'ErrorController@replyEdit')->name('errors.replyEdit');
	Route::put('errors/update/reply/{id}', 'ErrorController@replyUpdate')->name('errors.replyUpdate');
	Route::delete('errors/destroy/reply/{id}', 'ErrorController@replyDestroy')->name('errors.replyDestroy');
	Route::resource('errors', 'ErrorController');
});

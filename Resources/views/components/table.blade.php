<table id="{{ $id or 'table' }}" data-url="{{ $url or '' }}"
       data-attributes="{{ $attributes or '' }}"
       class="table table-striped table-bordered responsive dataTable {{ $class or '' }}"
       width="100%">
    <thead>
    <tr>
        {{ $slot }}
    </tr>
    </thead>
</table>

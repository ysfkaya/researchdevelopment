<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ asset('vendor/researchdevelopment/tables/js/jquery.dataTables.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('vendor/researchdevelopment/tables/js/datatable/dataTables.bootstrap4.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('vendor/researchdevelopment/tables/js/datatable/dataTables.responsive.min.js') }}"
        type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->

@if(isset($__buttons))
    <script src="{{ asset('vendor/researchdevelopment/tables/js/datatable/dataTables.buttons.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('vendor/researchdevelopment/tables/js/buttons.flash.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('vendor/researchdevelopment/tables/js/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/researchdevelopment/tables/js/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/researchdevelopment/tables/js/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/researchdevelopment/tables/js/buttons.html5.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('vendor/researchdevelopment/tables/js/buttons.print.min.js') }}"
            type="text/javascript"></script>
@endif

### Researching and Development for Penta Yazılım

This module created for Penta Yazılım company


## Installation

```
composer require ysfkaya/researchdevelopment
```

## Configure

Publish config :

```
php artisan vendor:publish --provider="Penta\ResearchDevelopment\Providers\ResearchDevelopmentServiceProvider" --tag=config
```

Publish public :

```
php artisan vendor:publish --provider="Penta\ResearchDevelopment\Providers\ResearchDevelopmentServiceProvider" --tag=public
```

## Config File Example

```
return [
    // Site Name
    'name'       => 'Application Name',
    // Site Address
    'href'       => 'example.com',

    // Middleware
    'middleware' => ['web','auth:admin'],

    // Route prefix
    'prefix'     => 'dashboard/arge',

    // View Layouts
    'view'       => [
        'extends' => 'layouts.admin',
        'body'    => 'content',
        'js'      => 'js',
        'css'     => 'css',
    ],
    // When created Development or Error Report send mail
    'mails'      => [
        'johndoe@hotmail.com'
    ],

    'roles' => [
        'developer' => 'role_1',
        'owner'     => 'role_2',
        'couple'    => ['role_1','role_2'],
    ],

    'storage_disk' => 'public',

    'user' => [
        'class'        => \App\User::class,
        'table'        => 'users',
        'reference_id' => 'user_id',
    ],

    'admin' => [
        'class'        => \App\User::class,
        'table'        => 'users',
        'reference_id' => 'admin_id',
    ],
];

```

### Usage

Add  the following code block in your admin panel to sidebar

```
<li class="navigation-header">
    <span>AR-GE</span>
    <i data-toggle="tooltip" data-placement="right" data-original-title="AR-GE" class=" ft-minus"></i>
</li>

<li class="nav-item">
    <a href="{{ route('arge.development.index') }}" class="menu-item">
        <i class="fa fa-cog"></i>
        <span data-i18n="" class="menu-title">
            Geliştirme Notları
        </span>

    </a>
</li>

<li class="nav-item">
    <a href="{{ route('arge.errorReports.index') }}" class="menu-item">
        <i class="fa fa-bug"></i>
        <span data-i18n="" class="menu-title">
            Hata Raporları
        </span>
    </a>
</li>

```

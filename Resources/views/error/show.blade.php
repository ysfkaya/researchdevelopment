@extends(config('researchdevelopment.view.extends'))

@section(config('researchdevelopment.view.body'))
    <div class="row">
        <div class="col-lg-8 col-md-12">
            <div class="card box-shadow-2">
                <div class="card-body">
                    <div class="ticket-title row">
                        <div class="col-md-8 col-lg-9">
                            <h3> {{ title_case($report->subject) }} </h3>
                        </div>
                        <div class="col-md-4 col-lg-3 ">
                            <!-- Check If the date is over 1 month or not -->
                            <p class="t-t-d">
                                {{ $report->created_at === $report->updated_at ?  $report->created_at->diffForHumans() : $report->updated_at->diffForHumans().' güncellendi' }}
                            </p>
                        </div>
                    </div>
                    <div class="ticket-detail">
                        <p>{!! $report->body !!}</p>
                    </div>
                    <div class="ticket-replies">

                        <div class="reply-form">
                            @include('researchdevelopment::error.reply')
                        </div>
                        <div class="all-replies">
                            @include('researchdevelopment::error.replies')

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="card box-shadow-2">
                <div class="card-body">
                    @include('researchdevelopment::error.info')
                </div>
            </div>
            <div class="card box-shadow-2">
                <div class="card-body">
                    @include('researchdevelopment::error.files',['width' => '55px'])
                </div>
            </div>
        </div>
    </div>

    @include('researchdevelopment::error.editReply')
@endsection

@section(config('researchdevelopment.view.js'))
    <script src="{{ asset('vendor/researchdevelopment/modalEffect/classie.js') }}"></script>
    <script src="{{ asset('vendor/researchdevelopment/modalEffect/modalEffects.js') }}"></script>
    <script src="{{ asset('vendor/researchdevelopment/global/js/report.js') }}"></script>
    <script src="{{ asset('vendor/researchdevelopment/global/global.js') }}"></script>
@endsection

@section(config('researchdevelopment.view.css'))
    <link rel="stylesheet" href="{{ asset('vendor/researchdevelopment/global/css/report.css') }}">
    <style>
        .reply-date {
            margin-left: 20px !important;
        }
    </style>
@endsection
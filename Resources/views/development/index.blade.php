@extends(config('researchdevelopment.view.extends'))

@section(config('researchdevelopment.view.body'))

    @component('researchdevelopment::components.content')

        @slot('head')
            Geliştirme Talebi
        @endslot

        @slot('buttons')
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#new_note">
                <i class="fa fa-plus-circle"></i>
                Yeni Not Ekle
            </button>
            @include('researchdevelopment::development.create')
            @include('researchdevelopment::development.edit')
            @include('researchdevelopment::development.show')
            @include('researchdevelopment::development.pricing')
            @include('researchdevelopment::development.showPrice')
        @endslot

        @slot('body')
            @component('researchdevelopment::components.table', ['url' => route('researchdevelopment.developments.data')])
                <tr>
                    <th data-data="title">Başlık</th>
                    <th data-data="status">Durum</th>
                    <th data-data="user.full_name" data-orderable="false" data-searchable="false">Oluşturan</th>
                    <th data-data="created_at">Oluşturma Tarih</th>
                    <th data-data="action" data-orderable="false" data-searchable="false" width="1%">Ayarlar</th>
                </tr>
            @endcomponent
        @endslot
    @endcomponent

@endsection

@section(config('researchdevelopment.view.css'))
    @include('researchdevelopment::assets.css.table')
    @include('researchdevelopment::assets.css.toastr')
@endsection

@section(config('researchdevelopment.view.js'))
    @include('researchdevelopment::assets.js.table')
    @include('researchdevelopment::assets.js.toastr')
    @include('researchdevelopment::inc.toastr')

    <script src="{{ asset('vendor/researchdevelopment/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('vendor/researchdevelopment/tinymce/langs/tr_TR.js') }}"></script>
    <script src="{{ asset('vendor/researchdevelopment/global/global.js') }}"></script>
    <script>
        var editor_config = {
            selector: 'textarea#researchdevelopment',
            branding: false,
            menubar: false,
            toolbar: 'undo redo | styleselect | bold italic | link'
        };
    </script>
    <script>
        $(function () {
            tinymce.init(editor_config);
        });
    </script>

    <script>
        $(function () {
            $('body').on('shown.bs.modal', '#edit_development', function () {
                $('#edit_development form').attr('action', $('.action_class').val());
            });
            $('body').on('shown.bs.modal', '#pricing', function () {
                $('#pricing form').attr('action', $('.pricing_action_class').val());
                $('#pricing form').attr('action', $('.pricing_action_class').val());
            });

            // show error note
            function showDevelopment(data) {
                var url = '{{ route('researchdevelopment.developments.show') }}';
                $.post(url, data, function (resp) {

                    if (resp.success) {

                        $('#show_note').find('.description').html(resp.description);
                        $('#show_note').find('.id').html(resp.id);
                        $('#show_note').find('.title').html(resp.title);
                        if (resp.image != null) {
                            $('#show_note').find('.image_text').html('Resim');
                            $('#show_note').find('.image').html("<a target='_blank' href='" + resp.image + "'><img  src='" + resp.image + "' width='566'/></a>");
                        } else {
                            $('#show_note').find('.image_text').html('');
                            $('#show_note').find('.image').html('');
                        }

                        $('#show_note').modal('show');
                    }

                }).fail(function (resp) {
                    toastr.error(resp.responseText, "Hata !");
                }).done(function (resp) {
                });
            }

            // review development data
            $('body').on('click', '.view_development', function () {

                var id = $(this).data('id');
                var data = {
                    _token: '{{ csrf_token() }}',
                    id: id
                };

                showDevelopment(data);

            });

            // on click development edit button
            $('body').on('click', '.edit_development', function (e) {
                e.preventDefault();
                var url = $(this).data('url');

                var $form = $('#edit_development').find('form');
                $.get(url, function (resp) {
                    $form.find('.action_class').val(resp.action);
                    $form.find('#title').val(resp.title);
                    $form.find('#row').val(resp.row);
                    $form.find('textarea#researchdevelopment_edit').val(resp.description);

                    editor_config.selector = 'textarea#researchdevelopment_edit';

                    tinymce.init(editor_config);

                    $('#edit_development').modal('show');

                }).fail(function () {
                    toastr.error("Sunucuya ulaşılamıyor.");
                });

            });


            // on click pricing button
            $('body').on('click', '.pricing', function (e) {
                e.preventDefault();
                var url = $(this).data('url');
                $('#pricing .pricing_action_class').val(url);
                $('#pricing').modal('show');
            });

            $('body').on('click', '.show_price', function (e) {
                e.preventDefault();
                var href = $(this).attr('href');
                $.get(href, function (response) {
                    $('#showPricing #price').val(response.development.price + ' ₺');
                    $('#showPricing #desc').html(response.development.price_desc);
                    $('#showPricing').modal('show');
                }).fail((error) => {

                    if (error.status === 403) {
                        toastr.warning('Bu işleme yetkiniz yok', 'İzin Yok', {
                            positionClass: "toast-bottom-right"
                        });
                    } else {
                        toastr.error('Beklenmeyen bir hata oluştu ! Tekrar deneyin...', '', {
                            positionClass: "toast-bottom-right"
                        })
                    }
                });
            });
        });
    </script>
    @if($errors->all())
        <script>
            $(function () {
                $('#new_note').modal('show');

            });
        </script>
    @elseif($errors->edit->all())
        <script>
            $(function () {
                $('#edit_development').modal('show');
            });
        </script>
    @elseif($errors->price->all())
        <script>
            $(function () {
                $('#pricing').modal('show');
            });
        </script>
    @endif
    <script src="{{ asset('vendor/researchdevelopment/tables/table.js') }}"></script>

@endsection
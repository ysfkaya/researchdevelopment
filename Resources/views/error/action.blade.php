@component('researchdevelopment::components.action')
    @role(config('researchdevelopment.roles.couple'))
    @slot('edit')
        <a href="{{ route('researchdevelopment.errors.edit',$report) }}"
           class="dropdown-item">
            <i class="fa fa-edit"></i>
            Düzenle
        </a>
    @endslot

    @slot('show')
        <a href="{{ route('researchdevelopment.errors.show',$report) }}"
           class="dropdown-item">
            <i class="fa fa-reply"></i>
            İncele
        </a>
    @endslot
    @endrole

    @role(config('researchdevelopment.roles.developer'))
    @slot('delete')
        <a class="dropdown-item __delete__researchdevelopment"
           href="{{ route('researchdevelopment.errors.destroy',$report) }}">
            <i class="fa fa-trash"></i>
            Sil
        </a>
    @endslot
    @endrole

@endcomponent

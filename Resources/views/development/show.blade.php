<!-- Modal: modalPoll -->
<div class="modal fade right" id="show_note" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <span>#</span>
                <span class="id font-weight-bold">1</span>
                <span class="">-</span>
                <span class="title font-weight-bold"></span>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">×</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <p><strong>Açıklama</strong></p>
                    <p class="description"></p>
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <button class="btn btn-outline-secondary cursor-pointer" data-dismiss="modal">Kapat</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal: modalPoll -->

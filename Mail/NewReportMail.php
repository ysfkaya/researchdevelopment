<?php

namespace Penta\ResearchDevelopment\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Penta\ResearchDevelopment\Entities\ErrorReport;

class NewReportMail extends Mailable implements ShouldQueue
{
	use Queueable, SerializesModels;

	public $report;

	/**
	 * Create a new message instance.
	 *
	 * @param \Penta\ResearchDevelopment\Entities\ErrorReport $report
	 */
	public function __construct(ErrorReport $report)
	{
		$this->report = $report;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->subject('Hata Raporu')->view('researchdevelopment::mail.report', ['content' => $this->report->body]);
	}
}
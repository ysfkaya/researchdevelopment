<?php

namespace Penta\ResearchDevelopment\Helper;

trait StatusTrait

{
	public function getStatusWithHtml()
	{
		switch ($this->attributes['status']) {
			case self::STATUS_PENDING_APPROVED:
				return $this->htmlButton('google', 'Onay Bekliyor');
			case self::STATUS_APPROVED:
				return $this->htmlButton('primary', 'Onaylandı');
			case self::STATUS_UNAPPROVED:
				return $this->htmlButton('amber bg-darken-4', 'Onaylanmadı');
			case self::STATUS_IN_PROCESS:
				return $this->htmlButton('blue-grey', 'İşlemde');
			case self::STATUS_SOLVED:
				return $this->htmlButton('success', 'Tamamlandı');
			case self::STATUS_UNSOLVED:
				return $this->htmlButton('pink', 'Yapılmadı');
			default:
				return $this->htmlButton('blue', 'Tanımsız');
		}
	}

	/**
	 * @param $color
	 * @param $title
	 *
	 * @return string
	 */
	protected function htmlButton($color, $title): string
	{
		return "<button class='btn btn-sm cursor-pointer btn-{$color}'>{$title}</button>";
	}

	public static function getActiveCount()
	{
		return static::where('status', '!=', self::STATUS_SOLVED)->where('status', '!=', self::STATUS_UNAPPROVED)->count();
	}
}
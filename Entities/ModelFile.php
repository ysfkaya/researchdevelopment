<?php

namespace Penta\ResearchDevelopment\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Penta\ResearchDevelopment\Helper\Report\Priority;
use Penta\ResearchDevelopment\Helper\Report\Status;

class ModelFile extends Model
{
	protected $guarded = [];

	protected $appends = [
		'width',
		'height',
	];

	/**
	 * @return \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
	 */
	public function getDisk()
	{
		return Storage::disk($this->attributes['disk']);
	}

	/**
	 * @return string
	 */
	public function getFile()
	{
		$type = $this->attributes['type'];

		if ($this->isImage($type)) {
			return $this->composeImage();
		}

		return $this->withIcon();
	}

	public function getFileForDetail(string $maxWidth = '865px')
	{
		$type = $this->attributes['type'];

		if ($this->isImage($type)) {
			return $this->composeImageForDetail($maxWidth);
		}

		return $this->withIconForDetail();
	}

	private function composeImage()
	{
		return $this->appendHtml(sprintf('<img width="100" height="50" class="rounded d-block float-md-left mr-2" src="%s"/>', $this->url));
	}

	private function withIcon()
	{
		return $this->appendHtml('<i class="fa fa-file font-large-2 mr-2"></i>');
	}

	private function composeImageForDetail(string $maxWidth)
	{
		return $this->appendHtmlForDetail(sprintf('<img style="max-width:%s" class="rounded mr-2 mb-2" src="%s"/>', $maxWidth, $this->url));
	}

	public function getUrlAttribute()
	{
		return $this->getDisk()->url('error/'.$this->file_name);
	}

	private function withIconForDetail()
	{
		$html = '<i class="fa fa-file font-large-5 mr-2"></i>';

		return $this->appendHtmlForDetail($html);
	}

	private function appendHtml($appendElement)
	{
		$html = '<a href="#" class="file-detail-show" data-id="'.$this->id.'">';
		$html .= $appendElement;
		$html .= $this->attributes['original_name'].'</a>';

		return $html;
	}

	private function appendHtmlForDetail($appendElement)
	{
		return sprintf('<a href="%s" target="_blank">%s</a>', $this->url, $appendElement);
	}

	private function isImage($type): bool
	{
		$startType = explode('/', $type);

		if ($startType[0] === 'image') {
			return true;
		}

		return false;
	}

	/**
	 * @param string $eventName
	 *
	 * @return string
	 */
	public function getDescriptionForEvent(string $eventName): string
	{
		switch ($eventName) {
			case 'updated':
				return "<strong>".$this->attributes['name']."</strong> adlı bir dosya düzenledi";
			case 'created':
				return "<strong>".$this->attributes['name']."</strong> adlı bir dosya ekledi";
			case 'deleted':
				return "Bir dosya sildi";
			default:
				return '';
		}
	}

	/**
	 * The "booting" method of the model.
	 *
	 * @return void
	 */
	protected static function boot()
	{
		parent::boot();

		static::deleted(function ($model) {

			if (\File::exists($model->path)) {
				\File::delete($model->path);
			}
		});
	}
}

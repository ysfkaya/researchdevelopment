<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorReportRepliesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('error_report_replies', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('error_report_id');
			$table->unsignedInteger(config('researchdevelopment.admin.reference_id'))->nullable();
			$table->unsignedInteger(config('researchdevelopment.user.reference_id'))->nullable();
			$table->text('reply_body');
			$table->string('ip_address');
			$table->timestamps();

			$table->foreign('error_report_id')->references('id')->on('error_reports')->onDelete('cascade');

			$table->foreign(config('researchdevelopment.admin.reference_id'))
				->references('id')
				->on(config('researchdevelopment.admin.table'))
				->onDelete('set null');

			$table->foreign(config('researchdevelopment.user.reference_id'))
				->references('id')
				->on(config('researchdevelopment.user.table'))
				->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('error_report_replies');
	}
}

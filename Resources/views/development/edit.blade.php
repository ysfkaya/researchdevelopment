<div class="modal fade text-left" id="edit_development" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            @if(count($errors->edit->all()) > 0)
                <ul class="alert alert-danger alert-dismissible mb-2 list-unstyled" role="alert">
                    @foreach($errors->edit->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Geliştirme Talebi Düzenle</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! BootForm::open()->class('form')->enctype('multipart/form-data')->put() !!}
            {!! BootForm::hidden('action')->class('action_class')  !!}
            <div class="modal-body">
                {!! BootForm::text('Başlık', 'title')->required() !!}
                {!! BootForm::textarea('Açıklama', 'edit_description')->required()->id('researchdevelopment_edit')->rows(50) !!}
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-outline-primary edit_submit_btn" value="Kaydet">
            </div>
            {!! BootForm::close() !!}
        </div>
    </div>
</div>
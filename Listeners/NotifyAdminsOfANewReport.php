<?php

namespace Penta\ResearchDevelopment\Listeners;

use Illuminate\Support\Facades\Mail;
use Penta\ResearchDevelopment\Events\ReportWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Penta\ResearchDevelopment\Mail\NewReportMail;

class NotifyAdminsOfANewReport implements ShouldQueue
{
	use InteractsWithQueue;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  ReportWasCreated $event
	 *
	 * @return void
	 */
	public function handle(ReportWasCreated $event)
	{
		Mail::to(config('researchdevelopment.mails'))->sendNow(new NewReportMail($event->report));
	}
}

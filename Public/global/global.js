(function (window, document, $) {
        'use strict';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const methods = {
            _alert: function (callback) {
                let $callback = callback || function () {
                };

                if (confirm('Emin misiniz ?')) {
                    $callback();
                }
            },
            _tableReload: function () {
                $('.dataTable').dataTable().api().ajax.reload();
            },
            _toastrSuccess: function (text, title) {

                let $text = text || 'İşlem başarılı';
                let $title = title || null;

                toastr.success($text, $title, {
                    positionClass: 'toast-bottom-right'
                });
            },
            _toastrError: function (text, title) {

                let $text = text || 'İşlem başarısız';
                let $title = title || null;

                toastr.error($text, $title, {
                    positionClass: 'toast-bottom-right'
                });
            },
            _incrementCount: function (element) {
                let $element = element || $('.trashed__count');
                let $current = parseInt($element.text());
                let $incremented = $current + 1;
                $element.html($incremented);
            },
            _decrementCount: function (element) {
                let $element = element || $('.trashed__count');
                let $current = parseInt($element.text());
                let $incremented = $current - 1;
                $element.html($incremented);
            },
            _delete: function (url, callback, data) {
                let $callback = callback || function () {
                };
                let $data = Object.assign({}, {_method: 'DELETE'}, data || {});

                this._post(url, $data, $callback);
            },
            _put: function (url, callback, data) {
                let $callback = callback || function () {
                };
                let $data = Object.assign({}, {_method: 'PUT'}, data || {});

                this._post(url, $data, $callback);
            },
            _post: function (url, data, callback) {
                let $callback = callback || function () {
                };

                if (typeof data === 'function') {
                    $callback = data;
                }

                startBlockUI();

                if (typeof data !== "function") {
                    $.post(url, data, $callback).always(function () {
                        stopBlockUI();
                    });
                } else {
                    $.post(url, $callback).always(function () {
                        stopBlockUI();
                    });
                }

            },
            _onClick: function (elem, fn) {
                fn = fn || function () {
                };

                $('body').on('click', elem, fn);
            },
            _openModal: function (elem) {
                elem.modal('show');
            },
            _loadingDOM: function () {
                return '<div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw fa-2x"></i></div>';
            },
            _handleRequestProcess: function (resp, that, successCallback, errorCallback) {
                let $sCallback = successCallback || function () {
                };
                let $eCallback = errorCallback || function () {
                };

                if (resp.error) {
                    this._toastrError(resp.message);
                    $eCallback();
                } else {
                    if (!that.hasClass('__notTable')) {
                        this._tableReload();
                    } else if (that.attr('redirect') !== undefined) {
                        setTimeout(function () {
                            window.location.href = that.attr('redirect');
                        }, 750);
                    }
                    this._toastrSuccess();
                    $sCallback();
                }
            }
        };

        methods._onClick('.__delete__researchdevelopment', function (e) {
            e.preventDefault();
            let $that = $(this);
            methods._alert(function () {
                methods._delete($that.attr('href'), (resp) => {
                    methods._handleRequestProcess(resp, $that);
                });
            });
        });
    }
)(window, document, jQuery);


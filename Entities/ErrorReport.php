<?php

namespace Penta\ResearchDevelopment\Entities;

use Illuminate\Database\Eloquent\Model;
use Penta\ResearchDevelopment\Helper\Report\Priority;
use Penta\ResearchDevelopment\Helper\Report\Status;

class ErrorReport extends Model implements Status, Priority
{
	protected $guarded = [];

	protected $appends = [
		'status_html',
		'priority_html',
		'last_reply',
		'last_reply_name',
	];

	public function files()
	{
		return $this->morphMany(ModelFile::class, 'model');
	}

	public function getStatusHtmlAttribute()
	{
		return $this->getStatus();
	}

	public function getPriorityHtmlAttribute()
	{
		return $this->getPriority();
	}

	public function getLastReplyAttribute()
	{
		return $this->createdAtHtmlParse();
	}

	public function getLastReplyNameAttribute()
	{
		$lastReply = $this->replies()->first();

		if ($lastReply !== null) {
			$customerOrUser = $lastReply->user === null ? $lastReply->admin : $lastReply->user;

			return $customerOrUser->full_name;
		}

		return 'Henüz Yanıtlanmamış';
	}

	public function user()
	{
		return $this->belongsTo(config('researchdevelopment.user.class'), config('researchdevelopment.user.reference_id'));
	}

	public function replies()
	{
		return $this->hasMany(ErrorReportReply::class, 'error_report_id')->latest();
	}

	public function getStatus(bool $html = true)
	{
		if ($html) {
			return $this->statusHtmlParse();
		}
	}

	private function statusHtmlParse()
	{
		$status = $this->attributes['status'];

		switch ($status) {
			case self::ON_HOLD:
				return $this->statusHtmlElement('Yanıt Bekliyor', 'danger');
			case self::ANSWER:
				return $this->statusHtmlElement('Yanıtlandı', 'facebook');
			case self::FINISHED:
				return $this->statusHtmlElement('Çözüldü', 'success');
			default:
				break;
		}
	}

	private function statusHtmlElement($text, $type = 'secondary')
	{
		return sprintf('<button class="btn btn-sm btn-%2$s">%1$s</button>', $text, $type);
	}

	public function getPriority(bool $html = true)
	{
		return $this->priorityHtmlParse($html);
	}

	private function priorityHtmlParse(bool $button = true)
	{
		$priority = $this->attributes['priority'];

		switch ($priority) {
			case self::PRIORITY_LOW:
				return $this->priorityHtmlElement('Düşük', 'primary', $button);
			case self::PRIORITY_MEDIUM:
				return $this->priorityHtmlElement('Normal', 'secondary', $button);
			case self::PRIORITY_HIGH:
				return $this->priorityHtmlElement('Yüksek', 'warning', $button);
			case self::PRIORITY_URGENT:
				return $this->priorityHtmlElement('Acil', 'danger', $button);
			default:
				break;
		}
	}

	private function priorityHtmlElement($text, $type, $button)
	{
		if ($button) {
			$html = '<button ';
			$html .= 'class="btn btn-sm btn-'.$type.'"';
			$html .= '>';
			$html .= $text;
			$html .= '</button>';
		} else {
			$html = '<span ';
			$html .= 'class="btn btn-sm btn-'.$type.'" style="cursor:default"';
			$html .= '>';
			$html .= 'Öncelik : '.$text;
			$html .= '</span>';
		}

		return $html;
	}

	private function createdAtHtmlParse()
	{
		$date = $this->replies()->latest()->first()->created_at ?? null;

		$html = '<span class="success">'.$date.'</span>';

		if (empty($date)) {
			$html = '<span class="danger">Henüz yanıtlanmamış</span>';
		}

		return $html;
	}

	public static function updateStatusAsAnswer($id)
	{
		static::findOrFail($id)->update([
			'status' => self::ANSWER,
		]);
	}

	public static function updateStatusOnHold($id)
	{
		static::findOrFail($id)->update([
			'status' => self::ON_HOLD,
		]);
	}
}

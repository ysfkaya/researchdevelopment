@extends(config('researchdevelopment.view.extends'))

@section(config('researchdevelopment.view.body'))

    @component('researchdevelopment::components.content')

        @slot('head')
            Hata Raporu Düzenle
        @endslot


        @slot('body')
            {!! BootForm::open()->action(route('researchdevelopment.errors.update',$report))->class('form')->enctype('multipart/form-data') !!}
            <input type="hidden" name="user_id" value="{{ $report->user_id }}">
            <div class="card-block">
                <div class="form-body">
                    {!! BootForm::text('Konu', 'subject')->required()->value($report->subject) !!}
                    {!! BootForm::select('Öncelik', 'priority')->required()->options($priorities)->select($report->priority) !!}
                    {!! BootForm::textarea('Açıklama', 'body')->required()->value($report->body) !!}
                    {!! BootForm::file('Dosya', 'files[]')->multiple()->class('form-control') !!}
                    @include('researchdevelopment::error.files')
                </div>
                <div class="form-actions center">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check-square-o"></i> Kaydet
                    </button>
                </div>
            </div>
            {!! BootForm::close() !!}
        @endslot
    @endcomponent

@endsection

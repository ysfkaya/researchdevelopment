@extends(config('researchdevelopment.view.extends'))

@section(config('researchdevelopment.view.body'))

    @component('researchdevelopment::components.content')

        @slot('head')
            Hata Raporları
        @endslot

        @slot('buttons')
            <a href="{{ route('researchdevelopment.errors.create') }}" class="btn btn-blue-grey">
                <i class="fa fa-plus-circle"></i>
                Rapor Oluştur
            </a>
        @endslot

        @slot('body')
            @component('researchdevelopment::components.table', ['url' => route('researchdevelopment.errors.data')])
                <tr>
                    <th data-data="id" data-orderable="false" data-searchable="false" data-visible="false">#</th>
                    <th data-data="subject">Konu</th>
                    <th data-data="status_html" data-name="status">Durum</th>
                    <th data-data="priority_html" data-name="priority">Öncelik</th>
                    <th data-data="last_reply" data-name="replies.created_at">Son Yanıt</th>
                    <th data-data="last_reply_name" data-orderable="false" data-searchable="false">Son Yanıtlayan</th>
                    <th data-data="created_at">Oluşturma Tarihi</th>
                    <th data-data="action" data-orderable="false" data-searchable="false" width="1%">Ayarlar</th>
                </tr>
            @endcomponent
        @endslot
    @endcomponent

@endsection

@section(config('researchdevelopment.view.css'))
    @include('researchdevelopment::assets.css.table')
    @include('researchdevelopment::assets.css.toastr')
@endsection



@section(config('researchdevelopment.view.js'))
    @include('researchdevelopment::assets.js.table')
    @include('researchdevelopment::assets.js.toastr')
    @include('researchdevelopment::inc.toastr')
    <script src="{{ asset('vendor/researchdevelopment/global/global.js') }}"></script>
    <script src="{{ asset('vendor/researchdevelopment/tables/table.js') }}"></script>
@endsection

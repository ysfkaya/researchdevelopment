@foreach($report->replies as $reply)
    @if(!empty($reply->user))
        <div class="single-reply">
            <div class="media">
                <div class="media-left reply-user-img">
                    {{--{!! $reply->user->htmlImage()  !!}--}}
                </div>
                <div class="media-body reply-user-details">
                    <h4 class="media-heading">
                        {{ $reply->user->full_name }}
                        <button class="remove-reply pull-right"
                                data-url="{{ route('researchdevelopment.errors.replyDestroy',$reply->id) }}">
                            <i class="fa fa-times"></i>
                        </button>
                    </h4>
                    <div class="row">
                        <p class="pull-left user-role">Müşteri </p>
                        <p class="pull-right reply-date">
                            {{ $reply->created_at->toDateTimeString() === $reply->updated_at->toDateTimeString() ? $reply->created_at->diffForHumans() : $reply->updated_at->diffForHumans().' güncellendi'}}
                        </p>
                    </div>
                    <div class="reply-message">
                        <p>{!! $reply->reply_body !!}</p>
                    </div>

                </div>
            </div>
        </div>
    @endif
    @if(!empty($reply->admin))
        <div class="single-reply">
            <div class="media">
                <div class="media-left reply-user-img">
                    {{--{!!  $reply->admin->htmlImage() !!}--}}
                </div>
                <div class="media-body reply-user-details">
                    <h4 class="media-heading">
                        {{ $reply->admin->full_name }}
                        <button class="edit-reply pull-right"
                                data-url="{{ route('researchdevelopment.errors.replyEdit',$reply->id) }}">
                            <i class="fa fa-edit"></i>
                        </button>
                        <button class="remove-reply pull-right"
                                onclick="return confirm('Emin misiniz ?')"
                                data-url="{{ route('researchdevelopment.errors.replyDestroy',$reply->id) }}">
                            <i class="fa fa-times"></i>
                        </button>
                    </h4>
                    <div class="row">
                        <p class="pull-left user-role">
                            {{  title_case(optional($reply->admin->roles()->first())->display_name)  }}
                        </p>
                        <p class="pull-right reply-date">
                            {{ $reply->getRepliedDate() }}
                        </p>
                    </div>
                    <div class="reply-message">
                        <p>{!! $reply->reply_body  !!} </p>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endforeach
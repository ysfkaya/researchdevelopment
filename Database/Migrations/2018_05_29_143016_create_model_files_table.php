<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelFilesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('model_files', function (Blueprint $table) {
			$table->increments('id');
			$table->morphs('model');
			$table->string('original_name');
			$table->string('file_name');
			$table->string('disk');
			$table->string('type');
			$table->string('extension');
			$table->string('size');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('model_files');
	}
}

<div class="modal fade text-left" id="showPricing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Fiyatlandırma</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                	<label class="control-label" for="price">Fiyat</label>
                    <input type="text"  id="price" disabled readonly class="form-control">
                </div>
                <div class="form-group">
                	<label class="control-label" for="desc">Açıklama</label>
                    <div readonly disabled class="form-control" id="desc">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php

namespace Penta\ResearchDevelopment\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ResearchDevelopmentServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerAssets();
		$this->registerConfig();
		$this->registerViews();
		$this->registerFactories();
		$this->loadMigrationsFrom(__DIR__.'/../Database/Migrations');
		$this->loadRoutesFrom(__DIR__.'/../Http/routes.php');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	public function registerAssets()
	{
		$this->publishes([
			__DIR__.'/../Public' => public_path('vendor/researchdevelopment'),
		], 'public');
	}

	/**
	 * Register config.
	 *
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
			__DIR__.'/../Config/config.php' => config_path('researchdevelopment.php'),
		], 'config');
		$this->mergeConfigFrom(__DIR__.'/../Config/config.php', 'researchdevelopment');
	}

	/**
	 * Register views.
	 *
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = resource_path('views/vendor/researchdevelopment');

		$sourcePath = __DIR__.'/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath,
		], 'views');

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path.'/penta/researchdevelopment';
		}, \Config::get('view.paths')), [$sourcePath]), 'researchdevelopment');
	}

	/**
	 * Register translations.
	 *
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = resource_path('lang/penta/researchdevelopment');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'researchdevelopment');
		} else {
			$this->loadTranslationsFrom(__DIR__.'/../Resources/lang', 'researchdevelopment');
		}
	}

	/**
	 * Register an additional directory of factories.
	 *
	 * @return void
	 */
	public function registerFactories()
	{
		if ( ! app()->environment('production')) {
			app(Factory::class)->load(__DIR__.'/../Database/factories');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}
}

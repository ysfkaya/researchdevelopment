<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevelopmentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('developments', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('user_id');
			$table->string('title');
			$table->text('description');
			$table->string('price')->nullable();
			$table->text('price_desc')->nullable();
			$table->string('status')->default('pending_pricing');
			$table->timestamps();

			if (Schema::hasTable('admins')) {
				$table->foreign('user_id')->references('id')->on('admins')->onDelete('cascade');
			} else {
				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('developments');
	}
}

{!! BootForm::open()->action(route('researchdevelopment.errors.reply',$report->id))->class('form reply_form') !!}

{!! BootForm::textarea('Yanıt Gönder ', 'reply_body')->required() !!}
{!! BootForm::file('Dosyalar', 'files[]')->multiple()->class('form-control') !!}

<div class="submit_status alert hidden"></div>

{!! BootForm::button('Gönder', 'null')->type('submit')->class('btn btn-success') !!}

{!! BootForm::close() !!}


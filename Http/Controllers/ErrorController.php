<?php

namespace Penta\ResearchDevelopment\Http\Controllers;

use App\Admin;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Penta\ResearchDevelopment\Entities\ErrorReport;
use Penta\ResearchDevelopment\Entities\ErrorReportReply;
use Penta\ResearchDevelopment\Entities\ModelFile;
use Penta\ResearchDevelopment\Events\ReportWasCreated;
use Penta\ResearchDevelopment\Helper\Report\Priority;
use Penta\ResearchDevelopment\Helper\Report\Status;
use Penta\ResearchDevelopment\Http\Requests\ReportRequest;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class ErrorController
 *
 * @package Penta\ResearchDevelopment\Http\Controllers
 */
class ErrorController extends Controller
{
	use ValidatesRequests, AuthorizesRequests, DispatchesJobs;

	/**
	 * @return mixed
	 */
	public function index()
	{
		return view('researchdevelopment::error.index');
	}

	/**
	 * @return mixed
	 */
	public function data()
	{
		$reports = ErrorReport::query()->with('replies');

		return DataTables::of($reports)
			->editColumn('subject', function ($report) {
				return sprintf('<a href="%s">%s</a>', route('researchdevelopment.errors.show', $report->id), $report->subject);
			})
			->filterColumn('status', $this->statusCallback())
			->filterColumn('priority', $this->priorityCallback())
			->addColumn('action', function ($report) {
				return view('researchdevelopment::error.action', compact('report'));
			})
			->rawColumns(['action', 'last_reply', 'status_html', 'priority_html', 'subject'])
			->make(true);
	}

	public function destroy($id)
	{
		$report = ErrorReport::findOrFail($id);

		$report->delete();

		return response()->json(['message' => 'Deleted']);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param $id
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function updatePriority(Request $request, $id)
	{
		$report = ErrorReport::findOrFail($id);

		$report->update(['priority' => $request->get('priority')]);

		return response()->json(['priority' => $report->getPriority()]);
	}

	/**
	 * @param $id
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function reply($id, Request $request)
	{
		$this->validate($request, ['reply_body' => 'required']);

		$attributes = $request->except('_token', 'files');
		$attributes['error_report_id'] = $id;

		if (Admin::getDeveloper()->id === auth()->id()) {
			$attributes['admin_id'] = auth()->id();
			$owner = false;
		} else {
			$attributes['user_id'] = auth()->id();
			$owner = true;
		}

		$attributes['ip_address'] = $request->ip();

		$reply = ErrorReportReply::create($attributes);

		$report = ErrorReport::findOrFail($id);

		$this->createFile($request, $report);

		ErrorReport::updateStatusOnHold($id);

		return response()->json(['reply_body' => $reply->body($owner)]);
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function show($id)
	{
		$report = ErrorReport::findOrFail($id);

		return view('researchdevelopment::error.show', compact('report'));
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function fileDestroy($id)
	{
		ModelFile::findOrFail($id)->delete();

		return back()->with('toastr_success', 'İşlem başarılı !');
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function create()
	{
		$priorities = $this->reportProporties();

		return view('researchdevelopment::error.create', compact('priorities'));
	}

	/**
	 * @param $id
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function edit($id)
	{
		$priorities = $this->reportProporties();

		$report = ErrorReport::findOrFail($id);

		return view('researchdevelopment::error.edit', compact('priorities', 'report'));
	}

	/**
	 * @param \Penta\ResearchDevelopment\Http\Requests\ReportRequest $request
	 * @param $id
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update(ReportRequest $request, $id)
	{
		$attributes = $request->except('files', '_token');
		$attributes['ip_address'] = $request->ip();

		$report = ErrorReport::findOrFail($id);

		$this->createFile($request, $report);

		return back()->with('toastr_success', 'İşlem başarılı');
	}

	/**
	 * @param \Penta\ResearchDevelopment\Http\Requests\ReportRequest $request
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store(ReportRequest $request)
	{
		$attributes = $request->except('files', '_token');
		$attributes['ip_address'] = $request->ip();

		$report = ErrorReport::create($attributes);

		$this->createFile($request, $report);

		event(new ReportWasCreated($report));

		return redirect()->route('researchdevelopment.errors.index')->with('toastr_success', 'İşlem başarılı');
	}

	/**
	 * @return \Closure
	 */
	private function statusCallback()
	{
		return function ($query, $keyword) {
			$sql = "IF(status = ?,'Yanıtlandı',";
			$sql .= "IF(status = ?,'Yanıt Bekliyor',";
			$sql .= "IF(status = ?,'Çözüldü',''))) like ?";

			$query->whereRaw($sql, [Status::ANSWER, Status::ON_HOLD, Status::FINISHED, '%'.$keyword.'%']);
		};
	}

	/**
	 * @return \Closure
	 */
	private function priorityCallback()
	{
		return function ($query, $keyword) {
			$sql = "IF(priority = ?,'Düşük',";
			$sql .= "IF(priority = ?,'Normal',";
			$sql .= "IF(priority = ?,'Yüksek',";
			$sql .= "IF(priority = ?,'Acil','')))) like ?";

			$query->whereRaw($sql, [
				Priority::PRIORITY_LOW,
				Priority::PRIORITY_MEDIUM,
				Priority::PRIORITY_HIGH,
				Priority::PRIORITY_URGENT,
				'%'.$keyword.'%',
			]);
		};
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 * @throws \Exception
	 */
	public function replyDestroy($id)
	{
		ErrorReportReply::findOrFail($id)->delete();

		return response()->json(['success' => true]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function replyEdit($id)
	{
		$reply = ErrorReportReply::findOrFail($id);

		return response()->json(['action' => route('researchdevelopment.errors.replyUpdate', $id), 'reply_body' => $reply->reply_body]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function replyUpdate($id, Request $request)
	{
		$reply = ErrorReportReply::findOrFail($id);

		$reply->update(['reply_body' => $request->get('reply_body')]);

		return back()->with('toastr_success', 'İşlem başarılı');
	}

	/**
	 * @param Request $request
	 * @param $report
	 */
	private function createFile(Request $request, $report): void
	{
		foreach ($request->file('files', []) as $file) {
			$storage = Storage::disk(config('researchdevelopment.storage_disk', 'public'));

			$storage->put('error/'.$report->id, $file);

			$attributes = [
				'original_name' => $file->getClientOriginalName(),
				'file_name'     => $file->hashName($report->id),
				'type'          => $file->getMimeType(),
				'extension'     => $file->getClientOriginalExtension(),
				'size'          => $file->getSize(),
				'disk'          => config('researchdevelopment.storage_disk', 'public'),
			];

			$report->files()->create($attributes);
		}
	}

	/**
	 * @return array
	 */
	private function reportProporties(): array
	{
		return [
			ErrorReport::PRIORITY_LOW    => 'Düşük',
			ErrorReport::PRIORITY_MEDIUM => 'Normal',
			ErrorReport::PRIORITY_HIGH   => 'Yüksek',
			ErrorReport::PRIORITY_URGENT => 'Acil',
		];
	}
}

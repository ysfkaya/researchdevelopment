@if(is_array($map))
    <optgroup label="{{ $key }}">
        @foreach($map as $index => $item)
            @include('components.map.recursive',['map' => $item,'key' => $index,'count' => ++$count])
        @endforeach
    </optgroup>
@else
    <option value="{{ $key }}" {{ old('section') == $key ? 'selected' : null  }}>{{ str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$count) }} {{ $map }}</option>
@endif

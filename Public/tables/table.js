window.table = window.table || {};
window.tableOptions = window.tableOptions || {};
$(function () {

    function startBlockUI() {
        $.blockUI({
            message: '<div>' +
            '<i class="fa fa-spinner fa-pulse fa-fw"></i>' +
            '</div>',
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.7,
                cursor: 'auto'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    }

    function stopBlockUI() {
        $.unblockUI();
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /***************************************
     *        js of column rendering        *
     ***************************************/

    window.tableOptions = {
        serverSide: true,
        processing: true,
        ajax: {
            url: $('.dataTable').data('url')
        },
        pageLength: 10,
        responsive: true,
        fnInitComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                    .text('Ara')
                    .prepend('<i class="fa fa-search" style="padding-right:5px"></i>')
                    .addClass('btn btn-success btn-sm mr-1 ml-1')
                    .click(function() {
                        self.search(input.val()).draw();
                    }),
                $clearButton = $('<button>')
                    .text('Temizle')
                    .addClass('btn btn-info btn-sm')
                    .prepend('<i class="fa fa-paint-brush" style="padding-right:5px"></i>')
                    .click(function() {
                        input.val('');
                        $searchButton.click();
                    });
            $('.dataTables_filter').append($searchButton, $clearButton);
            $('*[data-toggle="tooltip"]').tooltip();

        },
        language: {
            'sDecimal': ',',
            'sEmptyTable': 'Tabloda herhangi bir veri mevcut değil',
            'sInfo': '_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor',
            'sInfoEmpty': 'Kayıt yok',
            'sInfoFiltered': '(_MAX_ kayıt içerisinden bulunan)',
            'sInfoPostFix': '',
            'sInfoThousands': '.',
            'sLengthMenu': 'Sayfada _MENU_ kayıt göster',
            'sLoadingRecords': 'Yükleniyor...',
            'sProcessing': '',
            'sSearch': 'Ara:',
            'sZeroRecords': 'Eşleşen kayıt bulunamadı',
            'oPaginate': {
                'sFirst': 'İlk',
                'sLast': 'Son',
                'sNext': 'Sonraki',
                'sPrevious': 'Önceki'
            },
            'oAria': {
                'sSortAscending': ': artan sütun sıralamasını aktifleştir',
                'sSortDescending': ': azalan sütun sıralamasını aktifleştir'
            }
        }
    };

    var columns = [];

    $('.dataTable th').each(function (key, item) {
        columns.push($(item).data());
    });

    window.tableOptions.columns = columns;

    if (typeof dataTableOption !== 'undefined') {
        $.each(dataTableOption, function (key, value) {
            window.tableOptions[key] = value;
        });
    }

    window.table = $('.dataTable').on('processing.dt', function (e, settings, processing) {
        if (processing) {
            startBlockUI();
        } else {
            stopBlockUI();
        }

    }).dataTable(window.tableOptions);


    $('body').on('click', '.dataTable button:not(.no-action)', function() {
        var $value = $(this).text();
        $('.dataTables_filter input').val($value);
        $('.dataTable').DataTable().search($value).draw();
    });

    $('body').on('keydown', '.dataTables_filter input', function(e) {
        if (e.which == 13) {
            var $value = $(this).val();
            $('.dataTable').DataTable().search($value).draw();
        }
    });

    var $window = $(window);

    function tableElements() {
        $('.dataTables_paginate').addClass('mt-1');

        if ($window.width() < 768) {
            $('.dataTables_info').hide();
        } else {
            $('.dataTables_info').show();
        }
    }

    tableElements();
    $window.resize(tableElements());

});



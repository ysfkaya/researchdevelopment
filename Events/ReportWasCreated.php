<?php

namespace Penta\ResearchDevelopment\Events;

use Illuminate\Queue\SerializesModels;

class ReportWasCreated
{
	use SerializesModels;

	/**
	 * @var \Penta\ResearchDevelopment\Entities\ErrorReport
	 */
	public $report;

	/**
	 * Create a new event instance.
	 *
	 * @param $report
	 */
	public function __construct($report)
	{
		$this->report = $report;
	}
}

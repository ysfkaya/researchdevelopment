<div class="modal fade text-xs-left" id="editReply" tabindex="-1" role="dialog" aria-labelledby="myModalLabel5"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel5">Yanıt Düzenle</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            {!! BootForm::open()->action('')->put() !!}
            <div class="modal-body">
                <div class="form-group">
                    <textarea name="reply_body" cols="10" required rows="5" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-primary">Kaydet</button>
            </div>
            {!! BootForm::close() !!}
        </div>
    </div>
</div>
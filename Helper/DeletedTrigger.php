<?php

namespace Penta\ResearchDevelopment\Helper;

trait DeletedTrigger
{
	/**
	 * When destroy a data then delete file from the current model
	 *
	 * @return void
	 */
	public static function boot()
	{
		parent::boot();

		self::deleting(function ($model) {
			if (\File::exists($model->real_path)) {
				\File::delete($model->real_path);
			}
		});
	}
}
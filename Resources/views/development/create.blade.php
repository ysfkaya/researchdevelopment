<div class="modal fade text-left" id="new_note" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Yeni Geliştirme Talebi</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {!! BootForm::open()->action(route('researchdevelopment.developments.store'))->class('form') !!}
            <div class="modal-body">
                {!! BootForm::text('Başlık', 'title')->required() !!}
                {!! BootForm::textarea('Açıklama', 'description')->id('researchdevelopment')->rows(50) !!}
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-outline-primary" value="Kaydet">
            </div>
            {!! BootForm::close() !!}
        </div>
    </div>
</div>


<!-- Modal -->
@if($hasError = $errors->edit_note->all())
    <div class="has-error-note-edit"></div>
@endif
<div class="modal fade" id="edit_note" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        {!! BootForm::open()->class('form') !!}
        @php  $hidden = BootForm::hidden('action')->id('edit-action') @endphp
        {!! $hidden !!}

        <div class="modal-content">
            <div class="modal-header  mt-2">
                <h5 class="modal-title text-center mt-0 mb-0 m-auto">
                    Not Düzenle
                </h5>
                <button type="button" class="close ml-0 font-bold font-size-large position-relative cursor-pointer"
                        style="top: -25px;"
                        data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color: #E6F3F8">
                @if($errors->edit_note->has('edit_body'))
                    <p class="help-block danger">
                        {{ $errors->edit_note->first('edit_body') }}
                    </p>
                @endif
            </div>
            <div class="modal-footer mt-0 mb-0 m-auto border-0">
                <button type="submit" class="btn btn-danger round btn-min-width mr-1 mb-1 cursor-pointer">
                    Kaydet
                </button>
            </div>
        </div>
        {!! BootForm::close() !!}
    </div>
</div>



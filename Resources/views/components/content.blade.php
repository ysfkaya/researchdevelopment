<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-2">
            {{ $head or ''}}
        </h3>

        {{ $buttons or '' }}
    </div>
</div>

<div class="content-body">
    <div class="row mt-1">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        {{ $body or ''}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
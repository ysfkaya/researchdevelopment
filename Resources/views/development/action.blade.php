@component('researchdevelopment::components.action')
    @role(config('researchdevelopment.roles.couple'))
    @slot('edit')
        <a data-url="{{ route('researchdevelopment.developments.edit',$development->id) }}"
           class="dropdown-item edit_development">
            <i class="fa fa-edit"></i>
            Düzenle
        </a>
    @endslot

    @slot('show')
        <a data-id="{{ $development->id }}"
           class="dropdown-item view_development">
            <i class="fa fa-eye"></i>
            İncele
        </a>
    @endslot

    @slot('delete')
        <a data-action="{{ route('researchdevelopment.developments.destroy',$development->id) }}"
           class="dropdown-item __delete__researchdevelopment">
            <i class="ft-trash"></i>
            Sil
        </a>
    @endslot
    @endrole

    @if($development->status == $development::STATUS_PRICING && optional(auth()->user())->hasRole(config('researchdevelopment.roles.owner')))
        <div class="dropdown-divider"></div>
        <a href="{{ route('researchdevelopment.developments.update.status',['status' => $development::STATUS_APPROVED,'id' => $development->id]) }}"
           class="update_publish_state dropdown-item">
            <i class="fa fa-calendar-check-o success"></i>
            Onayla
        </a>
        <a href="{{ route('researchdevelopment.developments.update.status',['status' => $development::STATUS_UNAPPROVED,'id' => $development->id]) }}"
           class="dropdown-item update_publish_state">
            <i class="fa fa-calendar-times-o amber"></i>
            Onaylama
        </a>
    @endif

    @role(config('researchdevelopment.roles.developer'))

    @if($development->status == $development::STATUS_APPROVED || $development->status == $development::STATUS_IN_PROCESS)
        <div class="dropdown-divider"></div>
        @if($development->status == $development::STATUS_IN_PROCESS)
            <a href="{{ route('researchdevelopment.developments.update.status',['status' => $development::STATUS_SOLVED,'id' => $development->id]) }}"
               class="dropdown-item update_publish_state">
                <i class="fa fa-check-circle-o primary"></i>
                Tamamlandı Olarak İşaretle
            </a>
            <a href="{{ route('researchdevelopment.developments.update.status',['status' => $development::STATUS_UNSOLVED,'id' => $development->id]) }}"
               class="dropdown-item update_publish_state">
                <i class="fa fa-times-circle-o danger"></i>
                Yapılmadı Olarak İşaretle
            </a>
        @else
            <a href="{{ route('researchdevelopment.developments.update.status',['status' => $development::STATUS_IN_PROCESS,'id' => $development->id]) }}"
               class="dropdown-item update_publish_state">
                <i class="fa fa-spinner blue"></i>
                İşlemde Olarak İşaretle
            </a>
        @endif
    @else
        <div class="dropdown-divider"></div>
        <a data-url="{{ route('researchdevelopment.developments.updatePricing',$development->id) }}"
           class="dropdown-item pricing">
            <i class="fa fa-try google"></i>
            Fiyatlandır
        </a>
    @endif
    @endrole

@endcomponent





